//
//  Interactor.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import Foundation

/// Протокол класса бизнес-логики
protocol Interactable {

	/// Запустить отслеживание времени
	func startTimeTracking()
}

/// Протокол делегирования событий класса бизнес-логики
protocol InteractorDelegate: AnyObject {

	/// Пришло новое значение времени
	/// - Parameter newTimeString: обект бизнес-логики
	func timeUpdated(_ newTimeString: String)
}

/// Класс бизнес-логики
final class Interactor {

	private let timeNotifier: TimeNotifiable
	private var timer: Timer?

	/// Делегат событий бизнес-логики
	weak var delegate: InteractorDelegate?

	init(timeNotifier: TimeNotifiable) {
		self.timeNotifier = timeNotifier
		self.timeNotifier.setObservers([self])
	}
}

// MARK: - Interactable
extension Interactor: Interactable {
	func startTimeTracking() {
		timer = Timer.scheduledTimer(timeInterval: 0.2,
									 target: self,
									 selector: #selector(makeRequest),
									 userInfo: nil,
									 repeats: true)
	}
}

// MARK: - TimeNotifierObservable
extension Interactor: TimeNotifierObservable {
	func timeUpdated(_ newTime: String) {
		delegate?.timeUpdated(newTime)
	}
}

private extension Interactor {
	@objc func makeRequest() {
		timeNotifier.requestTime()
	}
}
