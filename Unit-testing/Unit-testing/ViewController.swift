//
//  ViewController.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import UIKit

/// Класс viewContoller-а
final class ViewController: UIViewController {

	@IBOutlet weak var timeLabel: UILabel?

	private let interactor: Interactable
	private let lock = NSLock()

	/// Переопределение иницилизатора ViewController-а
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
		let timeNotifier = TimeNotifier.init(restService: Rest(),
											 formatter: FormatService())
		let interactor = Interactor(timeNotifier: timeNotifier)
		self.interactor = interactor
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		interactor.delegate = self
	}

	/// Вспомагательный инициализатор (для тестов)
	/// - Parameter interactor: обект бизнес-логики
	init(interactor: Interactable) {
		self.interactor = interactor
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		let timeNotifier = TimeNotifier.init(restService: Rest(),
											 formatter: FormatService())
		let interactor = Interactor(timeNotifier: timeNotifier)
		self.interactor = interactor
		super.init(coder: coder)
		interactor.delegate = self
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		interactor.startTimeTracking()
	}
}

// MARK: - InteractorDelegate
extension ViewController: InteractorDelegate {
	func timeUpdated(_ newTimeString: String) {
		lock.lock()
		DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			self.timeLabel?.text = newTimeString
		}
		lock.unlock()
	}
}

