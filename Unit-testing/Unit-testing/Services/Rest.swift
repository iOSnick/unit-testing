//
//  Rest.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import Foundation

/// Проткол Rest-сервиса
protocol RestProtocol {

	/// Осуществить запрос на XML, представить его в виде UTF8
	func getDataAsUtf8WithCurrentTime(completionResult: @escaping (Result<String, Error>) -> Void)
}

/// Сервис работы с сетью
final class Rest {

	private var url: URL? {
		URL(string: "https://www.timeanddate.com/worldclock/russia/moscow")
	}
}

/// MARK: - RestProtocol
extension Rest: RestProtocol {
	func getDataAsUtf8WithCurrentTime(completionResult: @escaping (Result<String, Error>) -> Void) {
		guard let url = url else { return }

		DispatchQueue.global().async {
			let task = URLSession.shared.dataTask(with: url) { data, response, error in
				guard
					let data = data,
					let dataString = String(data: data, encoding: String.Encoding.utf8)
				else {
					guard let error = error else { return }
					completionResult(.failure(error))
					return
				}

				completionResult(.success(dataString))
			}
			task.resume()
		}
	}
}
