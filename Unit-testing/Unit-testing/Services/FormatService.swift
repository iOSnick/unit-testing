//
//  FormatService.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

/// Протокол работы с сервисом-форматтером
protocol FormatChangable {

	/// Распарсить XML-UTF8 строку, получить из нее время
	/// Returns: опционально - найденную подстроку
	func formatXMLDataAsTime(data: String) -> String?
}

/// Сервис форматирования
final class FormatService {}

// MARK: - FormatChangable
extension FormatService: FormatChangable {
	func formatXMLDataAsTime(data: String) -> String? {

		let targetString = "class=h1>"

		guard
			data.contains(targetString),
			let lastComponent = data.components(separatedBy: targetString).last,
			lastComponent.count > 7
		else { return nil }

		return String(lastComponent.prefix(8))
	}
}
