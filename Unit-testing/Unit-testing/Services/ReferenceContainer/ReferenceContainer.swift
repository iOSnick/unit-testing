//
//  ReferenceContainer.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

/// Контейнер со слабой ссылкой на объект
final class ReferenceContainer<T: AnyObject> {
	var value: T?

	init(_ value: T) {
		self.value = value
	}
}
