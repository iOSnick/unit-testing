//
//  TimeNotifier.swift
//  Unit-testing
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import Foundation

/// Протокол обекта TimeNotifier
protocol TimeNotifiable {

	/// Заменить наблюдателей
	func setObservers(_ observers: [TimeNotifierObservable])
	/// Добавить наблюдателей
	func addObservers(_ observers: [TimeNotifierObservable])
	/// Запросить время
	func requestTime()
}

/// Протокол наблюдателя за состоянием TimeNotifier
@objc protocol TimeNotifierObservable {
	/// Время обновилось
	/// - Parameter newTime: новое время в формате строки
	func timeUpdated(_ newTime: String)
}

/// Объект, ответственный за запрос времени (с возможностью подписки)
final class TimeNotifier {

	private let restService: RestProtocol
	private let formatter: FormatChangable

	private var observers = [ReferenceContainer<TimeNotifierObservable>]()

	/// Инициализатор объекта TimeNotifier
	/// - Parameters:
	///   - restService: Сервис работы с сетью
	///   - formatter: Форматтер для обработки данных от сети
	init(restService: RestProtocol,
		 formatter: FormatChangable) {
		self.restService = restService
		self.formatter = formatter
	}
}

// MARK: - TimeNotifiable
extension TimeNotifier: TimeNotifiable {

	func addObservers(_ observers: [TimeNotifierObservable]) {
		self.observers.append(contentsOf: observers.map{ ReferenceContainer($0) })
	}

	func setObservers(_ observers: [TimeNotifierObservable]) {
		self.observers.removeAll()
		self.observers.append(contentsOf: observers.map{ ReferenceContainer($0) })
	}

	func requestTime() {
		restService.getDataAsUtf8WithCurrentTime { [weak self] result in
			guard let self = self else { return }

			switch result {
			case .success(_):
				guard
					let timeXMLAsUtf8Data = try? result.get(),
					let timeString = self.formatter.formatXMLDataAsTime(data: timeXMLAsUtf8Data)
				else {
					self.notifyAll(with: "Ошибка парсинга")
					return
				}
				self.notifyAll(with: timeString)
			case .failure(let error):
				self.notifyAll(with: error.localizedDescription)
			}
		}
	}
}

private extension TimeNotifier {
	func notifyAll(with text: String) {
		observers.forEach { observer in
			guard let observerValue = observer.value else { return }
			observerValue.timeUpdated(text)
		}
	}
}
