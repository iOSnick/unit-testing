//
//  TimeNotifierTests.swift
//  Unit-testingTests
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import XCTest
@testable import Unit_testing

final class TimeNotifierTests: XCTestCase {

	var sut: TimeNotifier!
	var restProtocolSpy: RestProtocolSpy!
	var formatterSpy: FormatChangableSpy!
	var observer: TimeNotifierObservableSpy!

	override func setUp() {
		super.setUp()
		restProtocolSpy = RestProtocolSpy()
		formatterSpy = FormatChangableSpy()
		observer = TimeNotifierObservableSpy()
		sut = TimeNotifier(restService: restProtocolSpy,
						   formatter: formatterSpy)
	}

	override func tearDown() {
		restProtocolSpy = nil
		formatterSpy = nil
		sut = nil
		observer = nil
		super.tearDown()
	}

	func testTimeNotifierCommonCase() {
		// arrange
		observer = TimeNotifierObservableSpy()
		sut.setObservers([observer])
		formatterSpy.stubbedFormatXMLDataAsTime = "123"
		restProtocolSpy.stubbedResult = .success("text")

		// act
		sut.requestTime()

		// assert
		XCTAssertEqual(formatterSpy.catchedData, "text")
		XCTAssertEqual(observer.catchedNewTime, "123")
	}
}
