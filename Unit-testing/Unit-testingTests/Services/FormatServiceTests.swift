//
//  FormatServiceTests.swift
//  Unit-testingTests
//
//  Created by Сергей Вихляев on 06.03.2022.
//

import XCTest
@testable import Unit_testing

final class FormatServiceTests: XCTestCase {

	/// ТЕСТИРУЕМЫЙ ОБЪЕКТ - SUT
	var sut: FormatService!

	override func setUp() {
		super.setUp()
		sut = FormatService()
	}

	override func tearDown() {
		sut = nil
		super.tearDown()
	}

	func testSuccessCase1() {

		// arrange
		let testString = "class=h1>TESTSTRING"

		// act
		let substring = sut.formatXMLDataAsTime(data: testString)

		// assert
		XCTAssertEqual(substring, "TESTSTRI")
	}

	func testFailureCaseLess8() {

		// arrange
		let testString = "less8"

		// act
		let substring = sut.formatXMLDataAsTime(data: testString)

		// assert
		XCTAssertNil(substring)
	}
}
