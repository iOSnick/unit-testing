//
//  RestProtocolSpy.swift
//  Unit-testingTests
//
//  Created by Сергей Вихляев on 06.03.2022.
//

@testable import Unit_testing

/// Шпион, реализующий протокол RestProtocol
final class RestProtocolSpy {
	/// Застабленный результат псевдо-запроса
	var stubbedResult: (Result<String, Error>)?
}

// MARK: - RestProtocol
extension RestProtocolSpy: RestProtocol {
	func getDataAsUtf8WithCurrentTime(completionResult: @escaping (Result<String, Error>) -> Void) {
		guard let stubbedResult = stubbedResult else { return }
		completionResult(stubbedResult)
	}
}
