//
//  FormatChangableSpy.swift
//  Unit-testingTests
//
//  Created by Сергей Вихляев on 06.03.2022.
//

@testable import Unit_testing

final class FormatChangableSpy {

	var stubbedFormatXMLDataAsTime: String?
	var catchedData: String?
}

// MARK: - FormatChangable
extension FormatChangableSpy: FormatChangable {
	func formatXMLDataAsTime(data: String) -> String? {
		catchedData = data

		return stubbedFormatXMLDataAsTime
	}
}
