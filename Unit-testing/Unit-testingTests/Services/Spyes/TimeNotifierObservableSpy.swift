//
//  TimeNotifierObservableSpy.swift
//  Unit-testingTests
//
//  Created by Сергей Вихляев on 06.03.2022.
//

@testable import Unit_testing

final class TimeNotifierObservableSpy {
	var catchedNewTime: String?
}

// MARK: - TimeNotifierObservable
extension TimeNotifierObservableSpy: TimeNotifierObservable {
	func timeUpdated(_ newTime: String) {
		catchedNewTime = newTime
	}
}
